const link = document.createElement('a')

link.className = 'enlace'
link.id = 'nuevo'
link.setAttribute('href', 'https://google.com')
link.setAttribute('target', 'blank')
link.innerText = 'Google'

const text = document.createTextNode(' Link real')
link.appendChild(text)

link.textContent = 'Google'

//console.log(link)

const section = document.querySelector('.section')
//section.appendChild(link)

const img = section.lastElementChild
section.insertBefore(link, img)

const newHeading = document.createElement('h1')
newHeading.id = 'new-heading'
newHeading.appendChild(document.createTextNode('Nuevo Encabezado'))

const oldHeading = document.getElementById('title')

const parent = oldHeading.parentElement

parent.replaceChild(newHeading, oldHeading)

console.log(parent);

const links = document.querySelectorAll('nav a')

const deleteLinks = links[0]

deleteLinks.remove()

const otherLink = links[3]

otherLink.parentElement.removeChild(otherLink)

const firstLi = document.querySelector('ul li')

firstLi.className = 'list'
firstLi.classList.add('New-class')
firstLi.classList.remove('list')

firstLi.setAttribute('data-id', '3244')
firstLi.hasAttribute('data-id')
firstLi.removeAttribute('data-id')

console.log(firstLi);