//Utilidades para Winow
const saludo ='Hola!'
// window.console.log(saludo)
// alert(saludo)
// confirm('Si sí o si no')

//const respuesta = window.prompt('Una pregunta?')

//console.log(respuesta);

let anchura,
    altura

altura = window.innerHeight
anchura = window.innerWidth

//console.log(altura, anchura);

//Propiedad Location

let ubicacion = window.location

//ubicacion = location.search

//window.location.href = 'https://google.com'

//console.log(ubicacion);

//Propiedad History

console.log(history);
//history.go(-2)
//history.back()

//navigator

let navegador = window.navigator
navegador = navigator.appName // Actualmente no vale para nada
navegador = navigator.userAgent
navegador = navigator.language
console.log(navegador);