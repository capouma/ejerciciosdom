// //Formas no recomendadas
// const boton2 = document.querySelector('#boton-segundo')
// boton2.onclick = () => alert('sin saludo')

// // console.log(boton2);

// //Forma recomendada
// const titulo = document.querySelector('#titulo')
// titulo.addEventListener('click', () => alert('hoy vemos los eventos'))

// const saludar = () => alert('HOLA!')

// titulo.addEventListener ('click', saludar)

// //console.log(titulo);

// boton2.addEventListener ('click', () => titulo.removeEventListener('click', saludar))

// const botonBuscador = document.querySelector('#boton-buscador')
// //botonBuscador.addEventListener('click', () => confirm ('¿Buscar?'))

// const ejecutarBoton = e => {
//     e.preventDefault()
//     //confirm ('¿Buscar?')
//     let evento

//     evento = e
//     evento = e.type
//     evento = e.target
//     evento = e.target.id
//     evento = e.target.innerText
//     e.target.innerText = 'Buscando'

//     console.log(evento);
// }

// botonBuscador.addEventListener('click', ejecutarBoton)

// //console.log(botonBuscador);

//document.addEventListener('DOMContentLoaded', () => alert('HTML Cargado'))

const subtitulo = document.getElementById('subtitulo')

const obtenerEvento = e=> {
    console.log(`Evento ${e.type}`);
}

//subtitulo.addEventListener('click', obtenerEvento)
//subtitulo.addEventListener('dblclick', obtenerEvento)
//subtitulo.addEventListener('mouseover', obtenerEvento)
//subtitulo.addEventListener('mouseout', obtenerEvento)
//subtitulo.addEventListener('mousedown', obtenerEvento)
//subtitulo.addEventListener('mouseup', obtenerEvento)


const buscador = document.getElementById('buscador')
let obtenerBusqueda = e => {
    console.log(`Evento ${e.type} --------->`,buscador.value);
}

// buscador.addEventListener('keydown',obtenerBusqueda)
//buscador.addEventListener('keyup',obtenerBusqueda)
//buscador.addEventListener('focus',obtenerBusqueda)
//buscador.addEventListener('blur',obtenerBusqueda)
//buscador.addEventListener('cut',obtenerBusqueda)
//buscador.addEventListener('copy',obtenerBusqueda)
//buscador.addEventListener('paste',obtenerBusqueda)

obtenerBusqueda = () => {
    console.log(buscador.value);
}

buscador.addEventListener('input',obtenerBusqueda)

//console.log(buscador);

const form = document.getElementById('form-busqueda')

const enviarBusqueda = e => {
    e.preventDefault()
    //console.log(e.target.busqueda.value)
}
form.addEventListener ('submit', enviarBusqueda)

const card = document.querySelector('.card')
const info = document.querySelector('.info')
const eliminar = document.querySelector('.eliminar')



//card.addEventListener('click', () => console.log('click en card'))
// info.addEventListener('click', () => console.log('click en info'))
// eliminar.addEventListener('click', () => console.log('click en eliminar'))

// info.addEventListener('click', e => {
//     e.stopPropagation()
//     console.log('click en info')
// })

// eliminar.addEventListener('click', e => {
//     e.stopPropagation()
//     console.log('click en eliminar')
// })

// const eliminarElemento = e => {
//     e.preventDefault()
//     //console.log(e.target.classList);
//     if (e.target.classList.contains('enlace'))
//     {
//         console.log('YES');
//     }
//     else
//     {
//         console.log('NO');
//     }
// }

const eliminarElemento = e => {
    e.preventDefault()
    //console.log(e.target.classList);
    if (e.target.classList.contains('eliminar'))
    {
        e.target.parentElement.parentElement.remove()
    }
}


const main = document.querySelector('main')


main.addEventListener('click',eliminarElemento)